import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {AggregatorSdgComponent} from "./sdg.component";

@NgModule({
	imports: [
		RouterModule.forChild([
			{
				path: '',
				component: AggregatorSdgComponent,
				canDeactivate: [PreviousRouteRecorder]
			}
		])
	]
})
export class LibSdgRoutingModule { }