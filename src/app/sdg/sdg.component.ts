import {Component} from "@angular/core";
import {properties} from "../../environments/environment";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {SearchCustomFilter} from "../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {AggregatorInfo, PortalAggregators} from "../utils/aggregators";

@Component({
	selector: 'aggregator-sdg',
	template: `
		<sdg [customFilter]="customFilter"></sdg>
	`
})
export class AggregatorSdgComponent {
	aggregatorId;
	aggregator: AggregatorInfo;
	customFilter: SearchCustomFilter = null;

	constructor() {
		this.aggregatorId = ConnectHelper.getCommunityFromDomain(properties.domain);
    this.aggregator =  PortalAggregators.getFilterInfoByMenuId(this.aggregatorId);
		this.customFilter = PortalAggregators.getSearchCustomFilterByAggregator(this.aggregator);
	}

	public ngOnInit() {}
}
