import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LibSdgRoutingModule} from "./sdg-routing.module";
import {SdgRoutingModule} from "../openaireLibrary/sdg/sdg-routing.module";
import {SdgModule} from "../openaireLibrary/sdg/sdg.module";
import {AggregatorSdgComponent} from "./sdg.component";

@NgModule({
	imports: [
		CommonModule,
		LibSdgRoutingModule,
		SdgRoutingModule,
		SdgModule
	],
	declarations: [
		AggregatorSdgComponent
	],
	exports: [
		AggregatorSdgComponent
	],
	providers: [
		PreviousRouteRecorder
	]
})
export class LibSdgModule { }