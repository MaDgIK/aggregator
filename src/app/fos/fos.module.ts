import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LibFosRoutingModule} from "./fos-routing.module";
import {FosRoutingModule} from "../openaireLibrary/fos/fos-routing.module";
import {FosModule} from "../openaireLibrary/fos/fos.module";
import {AggregatorFosComponent} from "./fos.component";

@NgModule({
	imports: [
		CommonModule,
		LibFosRoutingModule,
		FosRoutingModule,
		FosModule
	],
	declarations: [
		AggregatorFosComponent
	],
	exports: [
		AggregatorFosComponent
	],
	providers: [
		PreviousRouteRecorder
	]
})
export class LibFosModule { }