import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {AggregatorFosComponent} from "./fos.component";

@NgModule({
	imports: [
		RouterModule.forChild([
			{
				path: '',
				component: AggregatorFosComponent,
				canDeactivate: [PreviousRouteRecorder]
			}
		])
	]
})
export class LibFosRoutingModule { }