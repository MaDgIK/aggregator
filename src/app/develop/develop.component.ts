import {Component, OnInit} from "@angular/core";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {properties} from "../../environments/environment";
import {Router} from "@angular/router";
import {AggregatorInfo, PortalAggregators} from "../utils/aggregators";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {Subscription} from "rxjs";
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";

@Component({
  selector: 'develop',
  template: `
		<div class="uk-section">
			<div class="uk-container uk-container-large">
				<h1>OpenAIRE APIs<br> for developers<span class="uk-text-primary">.</span></h1>
			</div>
			<div class="uk-section uk-container uk-container-large">
				<div class="uk-grid uk-grid-large uk-child-width-1-2@m" uk-grid>
					<div class="uk-text-center uk-margin-large-top">
						<div class="uk-width-2-3@m uk-margin-auto@m">
							<div class="uk-icon-bg-shadow uk-icon-bg-shadow-large uk-margin-auto">
								<icon name="description" customClass="uk-text-background" [flex]="true" ratio="2.5" type="outlined" visuallyHidden="For {{openaireEntities.RESULTS}}"></icon>
							</div>
							<h3>For {{openaireEntities.RESULTS | lowercase}}</h3>
							<div class="uk-margin-bottom">
								For {{openaireEntities.RESULTS | lowercase}} ({{openaireEntities.PUBLICATIONS | lowercase}}, {{openaireEntities.DATASETS | lowercase}}, {{openaireEntities.SOFTWARE | lowercase}} and {{openaireEntities.OTHER | lowercase}}) you can use the Graph API by adding the community parameter.
							</div>
							<a class="uk-display-inline-block uk-button uk-button-text"
									href="https://graph.openaire.eu/docs/apis/search-api/research-products" target="_blank">
								<span class="uk-flex uk-flex-middle">
									<span>Graph API</span>
								</span>
							</a>
						</div>
					</div>
					<div class="uk-margin-large-top">
						<div class="uk-margin-top">
							<div>Request examples:</div>
							<ul class="uk-list uk-list-large uk-list-bullet uk-list-primary">
								<li>
									<span>Access all </span><span class="uk-text-bolder">{{openaireEntities.RESULTS}}</span> ({{openaireEntities.PUBLICATIONS}}, {{openaireEntities.DATASETS}}, {{openaireEntities.SOFTWARE}}, {{openaireEntities.OTHER}})<br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="">https://api.openaire.eu/search/researchProducts?country={{aggregator.valueId}}</span>
								</li>
								<li>
									<span>Access </span><span class="uk-text-bolder">{{openaireEntities.PUBLICATIONS}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="">https://api.openaire.eu/search/publications?country={{aggregator.valueId}}</span>
								</li>
								<li>
									<span>Access </span> <span class="uk-text-bolder">Open Access {{openaireEntities.PUBLICATIONS}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">http://api.openaire.eu/search/publications?country={{aggregator.valueId}}&OA=true</span>
								</li>
								<li>
									<span>Access </span><span class="uk-text-bolder">{{openaireEntities.DATASETS}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">https://api.openaire.eu/search/datasets?country={{aggregator.valueId}}</span>
								</li>
								<li>
									<span>Access </span><span class="uk-text-bolder">{{openaireEntities.SOFTWARE}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">https://api.openaire.eu/search/software?country={{aggregator.valueId}}</span>
								</li>
								<li>
									<span>Access </span><span class="uk-text-bolder">{{openaireEntities.OTHER}}</span><br>
									<span class="uk-text-bold uk-margin-small-right">GET</span>
									<span class="uk-text-break">https://api.openaire.eu/search/other?country={{aggregator.valueId}}</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
  `,
  styleUrls: ['develop.component.css']
})
export class DevelopComponent implements OnInit {

  public aggregator: AggregatorInfo = null;
	public openaireEntities = OpenaireEntities;

  subs: Subscription[] = [];

  constructor(private seoService: SEOService,
              private _meta: Meta,
              private _router: Router,
              private _title: Title,  private _piwikService:PiwikService) {
  }
  public ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }
  ngOnInit() {
		let id = ConnectHelper.getCommunityFromDomain(properties.domain);
		this.aggregator = PortalAggregators.getFilterInfoByMenuId(id);
		if (this.aggregator) {
			/* Metadata */
			const url = properties.domain + properties.baseLink + this._router.url;
			this.seoService.createLinkForCanonicalURL(url, false);
			this._meta.updateTag({content: url}, "property='og:url'");
			const description = "Develop | " + this.aggregator.valueName;
			const title = "Develop | " + this.aggregator.valueName;
			this._meta.updateTag({content: description}, "name='description'");
			this._meta.updateTag({content: description}, "property='og:description'");
			this._meta.updateTag({content: title}, "property='og:title'");
			this._title.setTitle(title);
			if(properties.enablePiwikTrack && (typeof document !== 'undefined')){
				this.subs.push(this._piwikService.trackView(properties, "OpenAIRE").subscribe());
			}
		}else {
			this.navigateToError();
		}
  }

  private navigateToError() {
    this._router.navigate([properties.errorLink], {queryParams: {'page': this._router.url}});
  }

}
