import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import {OpenaireSearchRecommendedResultsForOrcidComponent} from "./searchRecommendedResultsForOrcid.component";
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";
import {SearchRecommendedResultsForOrcidModule} from "../../openaireLibrary/orcid/recommend-orcid-links/searchRecommendedResultsForOrcid.module";
import {SearchRecommendedResultsForOrcidRoutingModule} from "./searchRecommendedResultsForOrcid-routing.module";
import {FreeGuard} from "../../openaireLibrary/login/freeGuard.guard";
import {LoginGuard} from "../../openaireLibrary/login/loginGuard.guard";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchRecommendedResultsForOrcidModule,
    SearchRecommendedResultsForOrcidRoutingModule
  ],
  declarations: [
    OpenaireSearchRecommendedResultsForOrcidComponent
  ],
  exports: [
    OpenaireSearchRecommendedResultsForOrcidComponent
  ],
  // providers:    [PreviousRouteRecorder, IsRouteEnabled]
  providers:    [PreviousRouteRecorder, LoginGuard]
})
export class LibSearchRecommendedResultsForOrcidModule { }
