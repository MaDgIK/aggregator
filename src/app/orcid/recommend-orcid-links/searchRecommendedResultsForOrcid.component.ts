import {Component} from '@angular/core';

@Component({
  selector: 'openaire-search-recommended-results-for-orcid',
  template: `
    <search-recommended-results-for-orcid></search-recommended-results-for-orcid>
  `
})

export class OpenaireSearchRecommendedResultsForOrcidComponent {

  constructor() {}

  public ngOnInit() {}
}

