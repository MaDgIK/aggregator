import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {OpenaireSearchRecommendedResultsForOrcidComponent} from "./searchRecommendedResultsForOrcid.component";
import {FreeGuard} from "../../openaireLibrary/login/freeGuard.guard";
import {LoginGuard} from "../../openaireLibrary/login/loginGuard.guard";
import {properties} from "../../../environments/environment";

@NgModule({
  imports: [
    RouterModule.forChild([
      // { path: '', component: OpenaireSearchMyResultsInOrcidComponent, canActivate: [IsRouteEnabled], data: {
      { path: '', component: OpenaireSearchRecommendedResultsForOrcidComponent,
        canActivate: [LoginGuard], data: {
          redirect: properties.errorLink,  community : 'openaire'
        },
        canDeactivate: [PreviousRouteRecorder]
      }

    ])
  ]
})
export class SearchRecommendedResultsForOrcidRoutingModule { }
