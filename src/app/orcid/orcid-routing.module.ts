import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {OpenaireOrcidComponent} from './orcid.component';
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {properties} from "../../environments/environment";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OpenaireOrcidComponent,
        canActivate: [LoginGuard], data: {
          redirect: properties.errorLink,  community : 'openaire'
        },
        canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class OrcidRoutingModule { }
