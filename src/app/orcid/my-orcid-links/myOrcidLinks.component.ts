import {Component} from '@angular/core';

@Component({
  selector: 'openaire-my-orcid-links',
  template: `
    <my-orcid-links></my-orcid-links>   
  `
})

export class OpenaireMyOrcidLinksComponent {

  constructor() {}

  public ngOnInit() {}
}

