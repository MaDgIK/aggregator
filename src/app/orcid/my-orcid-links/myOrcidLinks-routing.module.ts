import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {OpenaireMyOrcidLinksComponent} from "./myOrcidLinks.component";
import {LoginGuard} from "../../openaireLibrary/login/loginGuard.guard";
import {properties} from "../../../environments/environment";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OpenaireMyOrcidLinksComponent,
        canActivate: [LoginGuard], data: {
          redirect: properties.errorLink,  community : 'openaire'
        },
        canDeactivate: [PreviousRouteRecorder]
      }

    ])
  ]
})
export class MyOrcidLinksRoutingModule { }
