import {Component} from '@angular/core';
import {properties} from "../../../environments/environment";

@Component({
    selector: 'openaire-dataset',
    template: `<result-landing type="dataset"></result-landing>`,
 })
export class OpenaireDatasetComponent{
}
