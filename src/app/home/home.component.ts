import {ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {Subscription, zip} from 'rxjs';
import {Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {ConfigurationService} from '../openaireLibrary/utils/configuration/configuration.service';
import {RefineFieldResultsService} from '../openaireLibrary/services/refineFieldResults.service';
import {OpenaireEntities, SearchFields} from '../openaireLibrary/utils/properties/searchFields';
import {RouterHelper} from '../openaireLibrary/utils/routerHelper.class';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {ErrorCodes} from '../openaireLibrary/utils/properties/errorCodes';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {Filter} from "../openaireLibrary/searchPages/searchUtils/searchHelperClasses.class";
import {AggregatorInfo, PortalAggregators} from "../utils/aggregators";
import {SearchCustomFilter} from "../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {properties} from "../../environments/environment";
import {portalProperties} from "../../environments/environment-aggregator";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {Numbers, NumbersComponent} from "../openaireLibrary/sharedComponents/numbers/numbers.component";

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.less']
})
export class HomeComponent {
  public portalName: string = "";
  public keyword: string = "";
  public searchFields: SearchFields = new SearchFields();
  public errorCodes: ErrorCodes = new ErrorCodes();
  public routerHelper: RouterHelper = new RouterHelper();
  public numbers: Numbers = {};
  properties: EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  public readMore: boolean = false;
  public funders = [];
  subs: Subscription[] = [];
  resultsQuickFilter: { filter: Filter, selected: boolean, filterId: string, value: string } = {
    filter: null,
    selected: true,
    filterId: "resultbestaccessright",
    value: "Open Access"
  };
  selectedEntity = "all";
  disableSelect;
  selectedEntitySimpleUrl;
  selectedEntityAdvancedUrl;
  resultTypes: Filter = {
    values: [],
    filterId: "type",
    countSelectedValues: 0,
    filterType: 'checkbox',
    originalFilterId: "",
    valueIsExact: true,
    title: "Result Types",
    filterOperator: "or"
  };
  public pageContents = null;
  customFilter: SearchCustomFilter = null;
  aggregatorId;
  aggregator: AggregatorInfo;
  entities = OpenaireEntities;
  @ViewChild('numbersComponent', {static: true}) numbersComponent: NumbersComponent;

  constructor(
      private _router: Router,
      private _refineFieldResultsService: RefineFieldResultsService,
      private _piwikService: PiwikService,
      private config: ConfigurationService, private _meta: Meta, private _title: Title, private seoService: SEOService,
      private helper: HelperService,
      private cdr: ChangeDetectorRef
  ) {
    this.aggregatorId = ConnectHelper.getCommunityFromDomain(properties.domain);
    this.aggregator = PortalAggregators.getFilterInfoByMenuId(this.aggregatorId);
    this.customFilter = PortalAggregators.getSearchCustomFilterByAggregator(this.aggregator);
    let description = "OpenAIRE Explore: Over 100M of research deduplicated, 170K research software, 11M research data. One of the largest open scholarly records collection worldwide.";
    let title = "OpenAIRE - Explore | " + this.aggregator.title;
    this._title.setTitle(title);
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
    this._meta.updateTag({content: title}, "property='og:title'");
  }

  private getPageContents() {
    this.subs.push(this.helper.getPageHelpContents(this.properties, 'openaire', this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }

  public ceil(num: number) {
    return Math.ceil(num);
  }

  public ngOnInit() {
    this.seoService.createLinkForCanonicalURL(this.properties.domain + this.properties.baseLink + this._router.url, false);
    this.getPageContents();
    if (this.properties != null) {
      var url = this.properties.domain + this.properties.baseLink + this._router.url;
      this._meta.updateTag({content: url}, "property='og:url'");
      if (this.properties.enablePiwikTrack && (typeof document !== 'undefined')) {
        this.subs.push(this._piwikService.trackView(this.properties, "OpenAIRE").subscribe());
      }
      if (this.numbersComponent) {
        this.numbersComponent.showPublications = portalProperties.entities.publication.isEnabled;
        this.numbersComponent.showDatasets = portalProperties.entities.dataset.isEnabled;
        this.numbersComponent.showSoftware = portalProperties.entities.software.isEnabled;
        this.numbersComponent.showOrp = portalProperties.entities.other.isEnabled;
        this.numbersComponent.showOrganizations = portalProperties.entities.organization.isEnabled;
        this.numbersComponent.showProjects = portalProperties.entities.project.isEnabled;
        this.numbersComponent.showDataProviders = portalProperties.entities.datasource.isEnabled;
      }
      this.subs.push(this.config.portalAsObservable.subscribe(data => {
            if (data) {
              if(data.name) {
                this.portalName = data.name;
              }
              var showEntity = {};
              for (var i = 0; i < data['entities'].length; i++) {
                showEntity["" + data['entities'][i]["pid"] + ""] = data['entities'][i]["isEnabled"];
              }
              if (this.numbersComponent) {
                this.numbersComponent.showPublications = !!showEntity["publication"];
                this.numbersComponent.showDatasets = !!showEntity["dataset"];
                this.numbersComponent.showSoftware = !!showEntity["software"];
                this.numbersComponent.showOrp = !!showEntity["orp"];
                this.numbersComponent.showProjects = !!showEntity["project"];
                this.numbersComponent.showDataProviders = !!showEntity["datasource"];
                this.numbersComponent.showOrganizations = !!showEntity["organization"];
              }
              if (this.numbersComponent && this.numbersComponent.showPublications) {
                this.resultTypes.values.push({
                  name: this.openaireEntities.PUBLICATIONS,
                  id: "publications",
                  selected: false,
                  number: 0
                });
              }
              if (this.numbersComponent && this.numbersComponent.showDatasets) {
                this.resultTypes.values.push({
                  name: this.openaireEntities.DATASETS,
                  id: "datasets",
                  selected: false,
                  number: 0
                });
              }
              if (this.numbersComponent && this.numbersComponent.showSoftware) {
                this.resultTypes.values.push({
                  name: this.openaireEntities.SOFTWARE,
                  id: "software",
                  selected: false,
                  number: 0
                });
              }
              if (this.numbersComponent && this.numbersComponent.showOrp) {
                this.resultTypes.values.push({
                  name: this.openaireEntities.OTHER,
                  id: "other",
                  selected: false,
                  number: 0
                });
              }
              if (this.numbersComponent) {
                this.numbersComponent.init(false, false, this.numbersComponent.showPublications, this.numbersComponent.showDatasets, this.numbersComponent.showSoftware, this.numbersComponent.showOrp, this.numbersComponent.showProjects, this.numbersComponent.showDataProviders, this.numbersComponent.showOrganizations,
                    this.customFilter ? StringUtils.URIEncode(this.customFilter.queryFieldName + " exact " + StringUtils.quote((this.customFilter.valueId))) : '');
              }
              this.getFunders();
            }
          },
          error => {
            this.handleError("Error getting community information", error);
          }
      ));
    }

  }

  public ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }

  private handleError(message: string, error) {
    console.error("Home Page: " + message, error);
  }

  entityChanged($event) {
    this.selectedEntity = $event.entity;
    this.selectedEntitySimpleUrl = $event.simpleUrl;
    this.selectedEntityAdvancedUrl = $event.advancedUrl;
  }

  goTo(simple: boolean) {
    let url = this.properties.searchLinkToAll;
    let parameterNames = [];
    let parameterValues = [];
    // if (this.selectedEntity == "result") {
    //   if (this.resultTypes) {
    //     let values = [];
    //     for (let value of this.resultTypes.values) {
    //       if (value.selected) {
    //         values.push(value.id);
    //       }
    //     }
    //     if (values.length > 0 && values.length != 4) {
    //       parameterNames.push("type");
    //       parameterValues.push(values.join(","));
    //     }
    //     if (this.resultsQuickFilter) {
    //       parameterNames.push("qf");
    //       parameterValues.push("" + this.resultsQuickFilter.selected);
    //     }
    //   }
    // } else if (this.selectedEntity == "all") {
    //   if (this.resultsQuickFilter) {
    //     parameterNames.push("qf");
    //     parameterValues.push("true");
    //   }
    // }
    if (this.keyword.length > 0) {
      parameterNames.push("fv0");
      parameterValues.push(this.keyword);
      parameterNames.push("f0");
      parameterValues.push("q");
    }
    if (this.customFilter) {
      parameterNames.push(this.customFilter.queryFieldName);
      parameterValues.push(this.customFilter.valueId);
      parameterNames.push("cf");
      parameterValues.push("true");
    }
    this._router.navigate([url], {queryParams: this.routerHelper.createQueryParams(parameterNames, parameterValues)});
  }

  getQueryParamsForAdvancedSearch(entity) {
    let params = {};
    if (entity == "result") {
      if (this.resultsQuickFilter) {
        params["qf"] = "" + this.resultsQuickFilter.selected;
      }
    }
    if (this.keyword.length > 0) {
      params["fv0"] = "" + this.keyword;
      params["f0"] = "q";
    }
    if (this.customFilter) {
      params = this.customFilter.getParameters(params);
    }
    return params;
  }

  getFunders() {
    let refineParams1 = '&fq=country%20exact%20%22CA%22';
    let refineParams2 = '&fq=resultbestaccessright%20exact%20%22Open%20Access%22&fq=country%20exact%20%22CA%22%20';
    this.subs.push(zip(
        this._refineFieldResultsService.getRefineFieldsResultsByEntityName(['relfunder'], 'result', this.properties, refineParams1),
        this._refineFieldResultsService.getRefineFieldsResultsByEntityName(['relfunder'], 'result', this.properties, refineParams2)
    ).subscribe((data: any[]) => {
      let queriedFunders1 = data[0][1][0].values;
      queriedFunders1.forEach(queriedFunder => {
        if (queriedFunder.id.includes('nserc')) {
          this.funders.push({
            "id": queriedFunder.id,
            "name": queriedFunder.name,
            "publications": queriedFunder.number,
            "openAccessPublications": null,
            "logo": 'assets/nserc_logo.png',
            "params": {
              relfunder: '"' + encodeURIComponent(queriedFunder.id) + '"'
            }
          });
        } else if (queriedFunder.id.includes('cihr')) {
          this.funders.push({
            "id": queriedFunder.id,
            "name": queriedFunder.name,
            "publications": queriedFunder.number,
            "openAccessPublications": null,
            "logo": 'assets/cihr_logo.png',
            "params": {
              relfunder: '"' + encodeURIComponent(queriedFunder.id) + '"'
            }
          });
        } else if (queriedFunder.id.includes('sshrc')) {
          this.funders.push({
            "id": queriedFunder.id,
            "name": queriedFunder.name,
            "publications": queriedFunder.number,
            "openAccessPublications": null,
            "logo": 'assets/sshrc_logo.png',
            "params": {
              relfunder: '"' + encodeURIComponent(queriedFunder.id) + '"'
            }
          });
        }
      });
      let queriedFunders2 = data[1][1][0].values;
      queriedFunders2.forEach(queriedFunder => {
        for (let funder of this.funders) {
          if (queriedFunder.id == funder.id) {
            funder.openAccessPublications = queriedFunder.number;
          }
        }
      });
    }));
  }

  isRouteAvailable(routeToCheck: string) {
    for (let i = 0; i < this._router.config.length; i++) {
      let routePath: string = this._router.config[i].path;
      if (routePath == routeToCheck) {
        return true;
      }
    }
    return false;
  }

  disableSelectChange(event: boolean) {
    this.disableSelect = event;
    this.cdr.detectChanges();
  }
}
