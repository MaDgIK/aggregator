import {NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {OpenaireErrorPageComponent} from './error/errorPage.component';
import {ConfigurationService} from "./openaireLibrary/utils/configuration/configuration.service";
import {PageURLResolverComponent} from "./openaireLibrary/utils/pageURLResolver.component";

const routes: Routes = [
  {path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)},
  {
    path: 'search/result',
    loadChildren: () => import('./landingPages/result/libResult.module').then(m => m.LibResultModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/publication',
    loadChildren: () => import('./landingPages/publication/libPublication.module').then(m => m.LibPublicationModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/dataset',
    loadChildren: () => import('./landingPages/dataset/libDataset.module').then(m => m.LibDatasetModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/software',
    loadChildren: () => import('./landingPages/software/libSoftware.module').then(m => m.LibSoftwareModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/other',
    loadChildren: () => import('./landingPages/orp/libOrp.module').then(m => m.LibOrpModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/project',
    loadChildren: () => import('./landingPages/project/libProject.module').then(m => m.LibProjectModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/dataprovider',
    loadChildren: () => import('./landingPages/dataProvider/libDataProvider.module').then(m => m.LibDataProviderModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/service',
    loadChildren: () => import('./landingPages/service/libService.module').then(m => m.LibServiceModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  {
    path: 'search/organization',
    loadChildren: () => import('./landingPages/organization/libOrganization.module').then(m => m.LibOrganizationModule), data: { showHeader: true, hasMenuSearchBar: true}
  },
  // Search Pages
  {
    path: 'search/find', loadChildren: () => import('./searchPages/find/libSearch.module').then(m => m.LibMainSearchModule)
  },
  {path: 'search/find/publications', component: PageURLResolverComponent},
  {path: 'search/find/datasets', component: PageURLResolverComponent},
  {path: 'search/find/software', component: PageURLResolverComponent},
  {path: 'search/find/other', component: PageURLResolverComponent},
  {
    path: 'search/find/:entity', loadChildren: () => import('./searchPages/find/libSearch.module').then(m => m.LibMainSearchModule)
  },
  {
    path: 'search/advanced/research-outcomes',
    loadChildren: () => import('./searchPages/advanced/searchResearchResults.module').then(m => m.OpenaireAdvancedSearchResearchResultsModule)
  },
  {
    path: 'search/advanced/organizations',
    loadChildren: () => import('./searchPages/advanced/advancedSearchOrganizations.module').then(m => m.LibAdvancedSearchOrganizationsModule)
  },
  {
    path: 'search/advanced/dataproviders',
    loadChildren: () => import('./searchPages/advanced/advancedSearchDataProviders.module').then(m => m.LibAdvancedSearchDataProvidersModule)
  },
  {
    path: 'search/advanced/services',
    loadChildren: () => import('./searchPages/advanced/advancedSearchServices.module').then(m => m.LibAdvancedSearchServicesModule)
  },
  {
    path: 'search/advanced/projects',
    loadChildren: () => import('./searchPages/advanced/advancedSearchProjects.module').then(m => m.LibAdvancedSearchProjectsModule)
  },
  {
    path: 'reload',
    loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule),
    data: {hasSidebar: false}
  },
	{
		path: 'sdgs',
		loadChildren: () => import('./sdg/sdg.module').then(m => m.LibSdgModule)
	},
	{
		path: 'fields-of-science',
		loadChildren: () => import('./fos/fos.module').then(m => m.LibFosModule), data: {extraOffset: 100}
	},
  {path: 'user-info', loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule)},
  {path: 'error', component: OpenaireErrorPageComponent},
  // ORCID Pages
  {path: 'orcid', loadChildren: () => import('./orcid/orcid.module').then(m => m.LibOrcidModule)},
  {path: 'my-orcid-links', loadChildren: () => import('./orcid/my-orcid-links/myOrcidLinks.module').then(m => m.LibMyOrcidLinksModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: "reload",
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
  subs = [];
  enabledRoutes =[];
  ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }
  constructor( private config: ConfigurationService, private router: Router){
    this.subs.push(this.config.portalAsObservable.subscribe(data => {
        if (data) {
          if (data['pages']) {
            for (var i = 0; i < data['pages'].length; i++) {
              this.enabledRoutes[data['pages'][i]['route']] = data['pages'][i]['isEnabled'];
            }
          }
          this.getOptionalRoutes();
        }
      },
      error => {
        // this.handleError('Error getting community information (e.g. pages,entities) for community with id: ' + this.communityId, error);
      }));
  }
  getOptionalRoutes(){
    let optionalRoutes: Routes = [
      // Deposit Pages
    { path: 'participate/deposit-datasets',  redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
    { path: 'participate/deposit-datasets-result',  redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
    { path: 'participate/deposit-subject-result',  redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
    { path: 'participate/deposit-publications',  redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
    { path: 'participate/deposit-publications-result',  redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
    
    { path: 'participate/deposit/learn-how', loadChildren: () => import('./deposit/deposit.module').then(m => m.LibDepositModule)},
    { path: 'participate/deposit/search', loadChildren: () => import('./deposit/searchDataprovidersToDeposit.module').then(m => m.LibSearchDataprovidersToDepositModule)},
    // Linking Pages
    { path: 'myclaims', loadChildren: () => import('./claims/myClaims/myClaims.module').then(m => m.LibMyClaimsModule)},
    { path: 'participate/claim', loadChildren: () => import('./claims/linking/linkingGeneric.module').then(m => m.LibLinkingGenericModule)},
    { path: 'participate/direct-claim', loadChildren: () => import('./claims/directLinking/directLinking.module').then(m => m.LibDirectLinkingModule)},
    {path: 'develop', loadChildren: () => import('./develop/develop.module').then(m => m.DevelopModule)}
    ];
    for (var i = 0; i <optionalRoutes.length; i++) {
      if(this.enabledRoutes[("/"+optionalRoutes[i].path)]){
        this.router.config.push(optionalRoutes[i]);
      }
    }
    this.router.config.push({path: '**', pathMatch: 'full', component: OpenaireErrorPageComponent});
    return routes;
  }
}
