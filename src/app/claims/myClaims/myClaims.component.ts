import {Component} from '@angular/core';
import {properties} from "../../../environments/environment";


@Component({
    selector: 'openaire-my-claims',
    template: `<my-claims *ngIf="claimsInfoURL" [claimsInfoURL]="claimsInfoURL"></my-claims>`
})
 export class OpenaireMyClaimsComponent {
  claimsInfoURL:string;

   public ngOnInit() {
     this.claimsInfoURL = properties.claimsInformationLink;
   }
}
