import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonBeta} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  environment: "beta",
  adminToolsPortalType: "aggregator",
  dashboard: "aggregator",
  enablePiwikTrack: true,
  useCache: false,
  useLongCache: true,
  showAddThis: true,
  statisticsAPIURL: "https://beta.services.openaire.eu/stats-api/",
  statisticsFrameAPIURL: "https://beta.openaire.eu/stats/",
  statisticsFrameNewAPIURL: "https://beta.services.openaire.eu/stats-tool/",
  useNewStatistisTool: true,
  claimsAPIURL: "https://beta.services.openaire.eu/claims-new/rest/claimsService/",
  bipFrameAPIURL: "https://bip.imsi.athenarc.gr/api/impact-chart?id=",
  searchAPIURLLAst: "https://beta.services.openaire.eu/search/v2/api/",
  searchResourcesAPIURL: "https://beta.services.openaire.eu/search/v2/api/resources",
  openCitationsAPIURL: "https://services.openaire.eu/opencitations/getCitations?id=",
  csvAPIURL: "https://beta.services.openaire.eu/search/v2/api/reports",
  searchCrossrefAPIURL: "https://api.crossref.org/works",
  searchDataciteAPIURL: "https://api.datacite.org/works",
  searchOrcidURL: "https://pub.orcid.org/v2.1/",
  orcidURL: "https://orcid.org/",
  orcidAPIURL: "https://services.openaire.eu/uoa-orcid-service/",
  orcidTokenURL : "https://orcid.org/oauth/authorize?",
  orcidClientId: "APP-IN0O56SBVVTB7NN4",
  myOrcidLinksPage: "/my-orcid-links",
  doiURL: "https://dx.doi.org/",
  pmcURL: "http://europepmc.org/articles/",
  pmidURL: "https://www.ncbi.nlm.nih.gov/pubmed/",
  handleURL: "http://hdl.handle.net/",
  cordisURL: "http://cordis.europa.eu/projects/",
  openDoarURL: "http://v2.sherpa.ac.uk/id/repository/",
  r3DataURL: "http://service.re3data.org/repository/",
  fairSharingURL: "https://fairsharing.org/",
  eoscMarketplaceURL: "https://marketplace.eosc-portal.eu/services/",
  sherpaURL: "http://sherpa.ac.uk/romeo/issn/",
  sherpaURLSuffix: "/",
  zenodo: "https://zenodo.org/",
  helpdesk: "https://www.openaire.eu/support/helpdesk",
  helpdeskEmail: "helpdesk@openaire.eu",
  utilsService: "https://demo.openaire.eu/utils-service",

  vocabulariesAPI: "https://beta.services.openaire.eu/provision/mvc/vocabularies/",

  piwikBaseUrl: "https://analytics.openaire.eu/piwik.php?idsite=",
  piwikSiteId: "553",
  cookieDomain: ".openaire.eu",

  feedbackmail: "feedback@openaire.eu",

  cacheUrl: "https://demo.openaire.eu/cache/get?url=",

  datasourcesAPI: "https://beta.services.openaire.eu/openaire/ds/search/",

  adminToolsCommunity: "aggregator",
  adminToolsAPIURL: "https://beta.services.openaire.eu/uoa-admin-tools/",
  useHelpTexts: false,
  contextsAPI: "https://beta.services.openaire.eu/openaire/context",
  communityAPI: "https://beta.services.openaire.eu/openaire/community/",

  csvLimit: 2000,
  pagingLimit: 20,
  resultsPerPage: 10,

  "baseLink" : "",
  "domain":"https://beta.canada.explore.openaire.eu",

  searchLinkToResult: "/search/result?id=",
  searchLinkToPublication: "/search/publication?articleId=",
  searchLinkToProject: "/search/project?projectId=",
  searchLinkToDataProvider: "/search/dataprovider?datasourceId=",
  searchLinkToService: "/search/service?serviceId=",
  searchLinkToDataset: "/search/dataset?datasetId=",
  searchLinkToSoftwareLanding: "/search/software?softwareId=",
  searchLinkToOrp: "/search/other?orpId=",
  searchLinkToOrganization: "/search/organization?organizationId=",

  searchLinkToAll: "/search/find/",
  searchLinkToPublications: "/search/find/publications",
  searchLinkToDataProviders: "/search/find/dataproviders",
  searchLinkToServices: "/search/find/services",
  searchLinkToProjects: "/search/find/projects",
  searchLinkToDatasets: "/search/find/datasets",
  searchLinkToSoftware: "/search/find/software",
  searchLinkToOrps: "/search/find/other",
  searchLinkToOrganizations: "/search/find/organizations",
  searchLinkToCompatibleDataProviders: "/search/content-providers",
  searchLinkToEntityRegistriesDataProviders: "/search/entity-registries",
  searchLinkToJournals: "/search/journals",
  searchLinkToResults: "/search/find/research-outcomes",

  searchLinkToAdvancedPublications: "/search/advanced/publications",
  searchLinkToAdvancedProjects: "/search/advanced/projects",
  searchLinkToAdvancedDatasets: "/search/advanced/datasets",
  searchLinkToAdvancedSoftware: "/search/advanced/software",
  searchLinkToAdvancedOrps: "/search/advanced/other",
  searchLinkToAdvancedDataProviders: "/search/advanced/dataproviders",
  searchLinkToAdvancedServices: "/search/advanced/services",
  searchLinkToAdvancedOrganizations: "/search/advanced/organizations",
  searchLinkToAdvancedResults: "/search/advanced/research-outcomes",

  errorLink: "/error",

  lastIndexInformationLink: "https://beta.openaire.eu/aggregation-and-content-provision-workflows",
  showLastIndexInformationLink: true,
  widgetLink: "https://beta.openaire.eu/index.php?option=com_openaire&view=widget&format=raw&projectId=",
  claimsInformationLink: "https://beta.openaire.eu/linking",
  lastIndexUpdate: "2020-05-11",
  indexInfoAPI: "https://beta.services.openaire.eu/openaire/info/",

  depositLearnHowPage: "/participate/deposit/learn-how",
  depositSearchPage: "/participate/deposit/search",
  altMetricsAPIURL: "https://api.altmetric.com/v1/doi/",
  reCaptchaSiteKey: "6LezhVIUAAAAAOb4nHDd87sckLhMXFDcHuKyS76P",
  footerGrantText : "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452"

};

export let properties: EnvProperties = {
  ...props,  ...common, ...commonBeta
}
